package br.com.jjss.microservice.loja.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InfoPedidoDTO {

	private Long id;
	private Integer tempoDePreparo;
	private String status;
	
}
