package br.com.jjss.microservice.loja.controller.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompraDTO {

	private List<ItemDaCompraDTO> itens;
	private EnderecoDTO endereco;
	
	
}
