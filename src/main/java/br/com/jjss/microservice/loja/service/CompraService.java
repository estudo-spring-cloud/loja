package br.com.jjss.microservice.loja.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jjss.microservice.loja.client.FornecedorClient;
import br.com.jjss.microservice.loja.controller.dto.CompraDTO;
import br.com.jjss.microservice.loja.controller.dto.InfoFornecedorDTO;
import br.com.jjss.microservice.loja.controller.dto.InfoPedidoDTO;
import br.com.jjss.microservice.loja.model.Compra;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CompraService {
/*
	@Autowired
	private RestTemplate client;
	
	@Autowired
	private DiscoveryClient eurekaClient;
	*/
	
	@Autowired
	private FornecedorClient fornecedorClient;
	
	public Compra realizaCompra(CompraDTO compra) {
		log.info("Buscando informações do fornecedor de {}", compra.getEndereco().getEstado());
		
		InfoFornecedorDTO infoFornecedor = fornecedorClient.getInfoPorEstado(compra.getEndereco().getEstado());
		log.info("Encontrado fornecedor. Endereço: {}", infoFornecedor.getEndereco());
		
		InfoPedidoDTO pedido = fornecedorClient.realizaPedido(compra.getItens());
		
		Compra compraSalva = Compra.builder()
				.pedidoId(pedido.getId())
				.tempoDePreparo(pedido.getTempoDePreparo())
				.enderecoDestino(compra.getEndereco().toString())
				.build();
		
		return compraSalva;
		
	}
	
	/*// exemplo com DiscoveryClient exibindo as instancias utilizadas no balancer
	public void realizaCompra(CompraDTO compra) {
		ResponseEntity<InfoFornecedorDTO> response = client.exchange("http://fornecedor/info/" + compra.getEndereco().getEstado()
				, HttpMethod.GET, null, InfoFornecedorDTO.class);
		
		eurekaClient.getInstances("fornecedor").stream().forEach(f -> {
			System.out.println(String.format("localhost:%d", f.getPort()));
		});
		
		System.out.println(response.getBody().getEndereco());
		
	}
	*/

}
